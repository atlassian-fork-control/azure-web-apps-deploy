#!/bin/bash

# Deploy web app with Azure app service
#
# Required globals:
#   APP_ID
#   PASSWORD
#   TENANT_ID
#   NAME
#   RESOURCE_GROUP
#   ZIP_FILE
#
# Optional globals:
#   SLOT
#   EXTRA_ARGS
#   DEBUG

source "$(dirname "$0")/common.sh"

enable_debug

# mandatory parameters
APP_ID=${APP_ID:?'APP_ID variable missing.'}
PASSWORD=${PASSWORD:?'PASSWORD variable missing.'}
TENANT_ID=${TENANT_ID:?'TENANT_ID variable missing.'}
NAME=${NAME:?'NAME variable missing.'}
RESOURCE_GROUP=${RESOURCE_GROUP:?'RESOURCE_GROUP variable missing.'}
ZIP_FILE=${ZIP_FILE:?'ZIP_FILE variable missing.'}

debug APP_ID: "${APP_ID}"
debug TENANT_ID: "${TENANT_ID}"
debug RESOURCE_GROUP: "${RESOURCE_GROUP}"
debug NAME: "${NAME}"
debug ZIP_FILE: "${ZIP_FILE}"

# auth
AUTH_ARGS_STRING="--username ${APP_ID} --password ${PASSWORD} --tenant ${TENANT_ID}"

if [[ "${DEBUG}" == "true" ]]; then
  AUTH_ARGS_STRING="${AUTH_ARGS_STRING} --debug"
fi

AUTH_ARGS_STRING="${AUTH_ARGS_STRING} ${EXTRA_ARGS:=""}"

debug AUTH_ARGS_STRING: "${AUTH_ARGS_STRING}"

info "Signing in..."

run az login --service-principal ${AUTH_ARGS_STRING}

# deployment
ARGS_STRING="--resource-group ${RESOURCE_GROUP} --name ${NAME} --src ${ZIP_FILE}"

if [[ ! -z "${SLOT}" ]]; then
  ARGS_STRING="${ARGS_STRING} --slot ${SLOT}"
fi

if [[ "${DEBUG}" == "true" ]]; then
  ARGS_STRING="${ARGS_STRING} --debug"
fi

ARGS_STRING="${ARGS_STRING} ${EXTRA_ARGS:=""}"

debug ARGS_STRING: "${ARGS_STRING}"

info "Starting deployment to Azure app service..."

run az webapp deployment source config-zip ${ARGS_STRING}

if [ "${status}" -eq 0 ]; then
  success "Deployment successful."
else
  fail "Deployment failed."
fi
